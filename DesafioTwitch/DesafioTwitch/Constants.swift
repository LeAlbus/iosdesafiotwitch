//
//  Constants.swift
//  DesafioTwitch
//
//  Created by Pedro Lopes on 19/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation

//Values
let resultsByRequest = 100
let clientID = "snlbd075nr1xecs562ptg9oyhrgrfy"

//API
let baseURL = "https://api.twitch.tv/kraken/games"
let manualURL = "https://www.twitch.tv/directory/game/"

//Endpoints
let topGames = "/top"

//DeepLinks
let openTwitchApp = "twitch://game/"

//Parameters
let fromPage = "offset"
let byPage = "limit"
let clientIDKey = "client_id"

//Strings
let rankingStr = "Ranking: Top "
let spectatorsStr = "Current spectators: "

