//
//  Game.swift
//  DesafioTwitch
//
//  Created by Pedro Lopes on 18/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation

struct GameList: Decodable{
    
    let list: [Game]
    let total: Int
    
    private enum CodingKeys: String, CodingKey{
        case list = "top"
        case total = "_total"
    }
}

struct Game: Decodable{
    
    let viewers: Int
    let info: Images
    
    private enum CodingKeys: String, CodingKey{
        case viewers = "viewers"
        case info = "game"
    }
}

struct Images: Decodable{
    
    let name: String
    let banner: Path
    let poster: Path
    
    private enum CodingKeys: String, CodingKey{
        case name = "name"
        case banner = "logo"
        case poster = "box"
    }
}

struct Path: Decodable{
    
    let path: String
    
    private enum CodingKeys: String, CodingKey{
        case path = "large"
    }
}
