//
//  GameDetailsViewController.swift
//  DesafioTwitch
//
//  Created by Pedro Lopes on 19/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class GameDetailsViewController: UIViewController{
 
    @IBOutlet weak var spectators: UILabel!
    
    @IBOutlet weak var gameTitle: UILabel!
    
    @IBOutlet weak var gamePoster: UIImageView!
    
    var gameInfo: Game?
    
    func setData(_ gameData: Game){
        self.gameInfo = gameData
    }
    
    override func viewDidLoad() {
        self.setupView()
    }
        
    func setupView(){
        
        if self.gameInfo != nil{
            self.gameTitle.text = self.gameInfo!.info.name
            self.spectators.text = spectatorsStr + String(self.gameInfo!.viewers)
        
            let imagePath = self.gameInfo?.info.poster.path
            let placeholderImage = UIImage(named: "twitchIcon")!
        
            self.gamePoster.af_setImage(withURL: URL(string: imagePath!)!, placeholderImage: placeholderImage)
        }
    }
    @IBAction func shareGame(_ sender: Any) {
        
        let gameURL = (manualURL + (gameInfo?.info.name)!).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let activityViewController = UIActivityViewController(activityItems: [gameURL ?? "https://www.twitch.tv/"], applicationActivities: nil)
        //Added default twitch homepage url in case there is some error on game url
        
        //removing email and message once they are not "social sharing"
        activityViewController.excludedActivityTypes = [UIActivityType.mail]
        activityViewController.excludedActivityTypes = [UIActivityType.message]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func viewOnTwitch(_ sender: Any) {
        let gameURLString = openTwitchApp + gameInfo!.info.name
        let formattedGameURL = gameURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let twitchURL = URL(string: formattedGameURL!)
        
        if (UIApplication.shared.canOpenURL(twitchURL!)) {
            UIApplication.shared.open(twitchURL!, options: [:], completionHandler: {
                (success) in
                if (success)
                {
                    print("Oppening twitch app")
                }
                else
                {
                    print("Could not open twitch app")
                }
            })
        } else {
            print ("Twitch app not installed")
            
            if let gameName = gameInfo?.info.name{
                let fullURL = manualURL + gameName
                let formattedURL = fullURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

                UIApplication.shared.open(URL(string : formattedURL!)!, options: [:], completionHandler: { (status) in
                })
            }
        }
    }
}
