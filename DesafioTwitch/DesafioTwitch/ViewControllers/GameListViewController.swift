//
//  GameListViewController.swift
//  DesafioTwitch
//
//  Created by Pedro Lopes on 19/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit

class GameListViewController: UITableViewController{
    
    var currentResultPage = 0
    var selectedGameInfo: Game?
    
    override func viewDidLoad() {
        self.getNewList()
        self.refreshControl?.addTarget(self, action: #selector(refreshList(_:)), for: .valueChanged)
    }
    
    @objc private func refreshList(_ sender: Any) {
        APIHandler.sharedInstance.resetList()
        self.getNewList() 
    }
    
    func getNewList(){
        
        APIHandler.sharedInstance.requestGameList(resultPage: self.currentResultPage, completionHandler: {
            response in
            if response != nil{
                self.refreshControl?.endRefreshing()

                self.tableView.reloadData()
            } else{
                
                //Recursivelly asks for the list if it can not load
                self.getNewList()
            }
            
        })
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APIHandler.sharedInstance.gameList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "gameCell") as! GameCellController
        if !APIHandler.sharedInstance.gameList.isEmpty{
            cell.setupCell(APIHandler.sharedInstance.gameList[indexPath.row], position: indexPath.row)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedGame: Game = APIHandler.sharedInstance.gameList[indexPath.row]
        self.selectedGameInfo = selectedGame
        
        self.performSegue(withIdentifier: "showGameDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showGameDetails" && selectedGameInfo != nil{
            
            let detailsView = segue.destination as! GameDetailsViewController
            detailsView.setData(selectedGameInfo!)
        }
    }
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView){
       
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        
        let gamesAlreadyListed = APIHandler.sharedInstance.gameList.count
        let totalGamesToList = APIHandler.sharedInstance.totalGames
        
        if ((scrollOffset + scrollViewHeight) - 20 >= scrollContentSizeHeight && !APIHandler.sharedInstance.isGettingData && gamesAlreadyListed < totalGamesToList)
        {
            self.currentResultPage += 1
            APIHandler.sharedInstance.requestGameList(resultPage: self.currentResultPage){ response in
                
                self.tableView.reloadData()
            }
        } 
    }
}
