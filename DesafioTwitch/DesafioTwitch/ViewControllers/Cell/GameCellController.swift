//
//  GameCellController.swift
//  DesafioTwitch
//
//  Created by Pedro Lopes on 18/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Social

class GameCellController: UITableViewCell{

    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var gameRanking: UILabel!
    @IBOutlet weak var gameIcon: UIImageView!
    
    func setupCell(_ game: Game, position: Int){
        
        self.gameName.text = game.info.name
        self.gameRanking.text = rankingStr + String(position+1)
        //We are adding one because the value comes from an index, starting with zero
        
        //Using poster here because most of the games do not have a Banner image
        let imagePath = game.info.poster.path
        let placeholderImage = UIImage(named: "twitchIcon")!

        self.gameIcon.af_setImage(withURL: URL(string: imagePath)!, placeholderImage: placeholderImage)
    }
}
