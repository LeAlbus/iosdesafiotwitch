//
//  APIHandler.swift
//  DesafioTwitch
//
//  Created by Pedro Lopes on 18/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

import Foundation
import Alamofire

class APIHandler: NSObject{
    
    
    static let sharedInstance = APIHandler()
    var isGettingData = false
    
    var gameList: [Game] = []
    var totalGames: Int = 0
    
    func requestGameList(resultPage: Int, completionHandler: @escaping (_ successObject: GameList?) -> ()){
        
        self.isGettingData = true
        
        //Checking if we have less games to request than the number of requests we make
        var requestBy: Int

        if self.gameList.count + resultsByRequest > totalGames && totalGames > 0{
            requestBy = self.gameList.count - 1
        } else {
            requestBy = resultsByRequest*resultPage
        }
        
        Alamofire.request((baseURL+topGames), parameters: [clientIDKey: clientID, byPage: resultsByRequest, fromPage: requestBy]).responseJSON { response in
            switch response.result {
            case .success:
                do{
                    let data = response.data
                    let responseList = try JSONDecoder().decode(GameList.self, from: data!)
             
                    self.gameList.append(contentsOf: responseList.list)
                    
                    //We coult not track this value all the times, but once we have no control above the changes on the list, we will keep tracking this all the times.
                    self.totalGames = responseList.total
                    
                    completionHandler(responseList)
                    self.isGettingData = false
                    
                    
                } catch let error{
                    print ("Error while parsing response")
                    print (error)
                    
                    self.isGettingData = false
                    completionHandler(nil)
                    
                }
            case .failure(_):
                print ("Could not retrieve information from url")

                self.isGettingData = false
                completionHandler(nil)                
            }
        }
    }
    
    func resetList(){
        self.gameList.removeAll()
        self.totalGames = 0
    }
}
